//
//  AHAppDelegate.h
//  AngelHackVisualization
//
//  Created by Kien Tran on 6/1/13.
//
//

#import <Cocoa/Cocoa.h>

@interface AHAppDelegate : NSObject <NSApplicationDelegate, NSTableViewDataSource, NSTableViewDelegate> {
    NSMutableArray *resultsArray;
}

@property (assign) IBOutlet NSWindow *window;
@property (weak) IBOutlet NSTableView *tableView;

@end
