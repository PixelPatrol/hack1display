//
//  main.m
//  AngelHackVisualization
//
//  Created by Kien Tran on 6/1/13.
//
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
