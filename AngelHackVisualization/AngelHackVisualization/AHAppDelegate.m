//
//  AHAppDelegate.m
//  AngelHackVisualization
//
//  Created by Kien Tran on 6/1/13.
//
//

#import "AHAppDelegate.h"
#import "AFHTTPClient.h"
#import "AFHTTPRequestOperation.h"

@implementation AHAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
    
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:@"http://search.twitter.com/"]];
    
    [client getPath:[NSString stringWithFormat:@"search.json?rpp=%d&q=angelhacksing",500] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"Finish pulling to refresh..");
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:kNilOptions error:nil];
        NSArray *results = [jsonDict objectForKey:@"results"];
        resultsArray = [NSMutableArray arrayWithArray:results];
        
        NSLog(@"RESPONSE: %@\n",jsonDict);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        

        NSLog(@"FAILED TO FETCH TWEETS.");
        
    }];
}

#pragma mark NSTableView

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
    return 2;
}

/* This method is required for the "Cell Based" TableView, and is optional for the "View Based" TableView. If implemented in the latter case, the value will be set to the view at a given row/column if the view responds to -setObjectValue: (such as NSControl and NSTableCellView).
 */
- (id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
    NSView *view = [[NSView alloc] initWithFrame:CGRectMake(0, 0, 320, 320)];
    return view;
}



@end
